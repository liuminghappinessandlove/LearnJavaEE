package chapter8collectionanditerator;

import java.util.ArrayList;
import java.util.Collection;

public class CollectionStream {

	public static void main(String[] args) {
		Collection courseList1 = new ArrayList();		
		courseList1.add("数据库");
		courseList1.add("JavaEE");
		courseList1.add("JavaEE");
		courseList1.add("高等数学");
		courseList1.add("大学英语");
		courseList1.add("线性代数");
		courseList1.add("线性代数");
		
		// 中间方法filter的使用和末端方法count的使用,统计出包含"Java"子串的个数
		// System.out.println(courseList1.stream().filter(a -> ((String)a).contains("Java")).count());
		
		// 中间方法mapToXXX方法和末端方法forEach的使用，将课程名的长度映射到新的Stream中并进行输出
		// courseList1.stream().mapToInt(course -> ((String)course).length())
			// .forEach(len -> System.out.println(len));
		
		// 末端方法anyMatch的使用，确定Stream中课程名的长度是否至少有一个大于4
		// System.out.println(courseList1.stream().anyMatch(course -> ((String)course).length() > 4));
		
		// 末端方法allMatch的使用，确定Stream中课程名的长度是否全部大于4
		// System.out.println(courseList1.stream().allMatch(course -> ((String)course).length() > 4));
		
		// 末端方法noneMatch的使用，确定Stream中课程名的长度是否没有一个大于4
		// System.out.println(courseList1.stream().noneMatch(course -> ((String)course).length() > 4));
		
		// 末端方法toArray的使用，将Stream中的课程转化成字符串数组
//		Object [] co = courseList1.stream().toArray();
//		for (Object c : co){
//			System.out.println(c);
//		} 
		
		// 末端方法findAny的使用，返回Stream中任意一个元素
		// System.out.println(courseList1.stream().findAny().get());
		
		// 短路，有状态的方法limit(限制最大输出个数，限制排在后面的数据不输出)和有状态的方法distinct(去除重复)使用,排除重复重复课程，并按课程名长度由小到大排列
		courseList1.stream()
			.distinct()
			.limit(3)
			.sorted(new CourseComparatorImp())
			.forEach(course -> System.out.println(course));
	}

}
