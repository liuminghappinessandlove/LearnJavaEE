package chapter8collectionanditerator;

import java.util.stream.IntStream;

public class IntStreamTest {

	public static void main(String[] args) {
		IntStream is = IntStream.builder()
		    .add(2)
		    .add(33)
		    .add(12)
		    .add(34)
		    .build();
		
		// 下面的方法只能选择一个调用，因为每个Stream操作只能执行一次
		// System.out.println("取最大值：" + is.max().getAsInt());
		// System.out.println("取最小值：" + is.min().getAsInt());
		// System.out.println("求和：" + is.sum());
		// System.out.println("求总个数：" + is.count());
		// System.out.println("取平均值：" + is.average().getAsDouble());
		// System.out.println("所有数的平方是否都大于20（全部都要成立）：" + is.allMatch(a -> a * a > 20));
		// System.out.println("是否包含任何数的平方都大于20(有一个成立就为true)：" + is.anyMatch(a -> a*a > 20));
		// 将is映射成一个新Stream,新Stream的每个数是原来的两倍
		IntStream newIs = is.map(a -> a * 2);
		//遍历Stream中的元素,也是只能执行一次
		newIs.forEach(a -> System.out.println(a));
		

	}

}
