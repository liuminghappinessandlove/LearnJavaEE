package chapter8collectionanditerator;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;

public class IteratorTest {

	public static void main(String[] args) {
		Collection courseList1 = new ArrayList();
		courseList1.add("数据库");
		courseList1.add("JavaEE");
		courseList1.add("高等数学");
		courseList1.add("大学英语");
		courseList1.add("线性代数");
		
		Iterator it = courseList1.iterator();
		
		while (it.hasNext()){
			// next的返回值为object,需要做强制类型转换
			String o = (String)it.next();
			if (o.equals("大学英语")) {
				// 使用迭代器进行集合元素的迭代时，不能用集合的引用删除元素，可以使用迭代器提供的remove方法进行删除
				// 使用迭代器进行集合元素的迭代时，不能修改集合数据，因为用的是原集合的引用
				it.remove();
			}
		}
		
		// 使用 迭代器中的forEachRemaining函数(it.forEachRemaining(course -> System.out.println(course));)在此处只会接着遍历it还没有遍历的元素，在此处输出就为空了
		courseList1.forEach(course -> System.out.println(course));
		
	}

}
