package chapter8collectionanditerator;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;

public class CollectionTest {

	public static void main(String[] args) {
		Collection courseList1 = new ArrayList();		
		courseList1.add("数据库");
		courseList1.add("JavaEE");
		courseList1.add("高等数学");
		courseList1.add("大学英语");
		courseList1.add("线性代数");
		
		Collection courseList2 = new HashSet();
		courseList2.add("大学英语");
		courseList2.add("线性代数");
		
//		System.out.println("打印输出courseList1中所有课程：" + courseList1);
//		System.out.println("courseList1中有几门课：" + courseList1.size());
//		System.out.println("courseList1中是否为空：" + courseList1.isEmpty());
//		System.out.println("courseList1中是否有数据库：" + courseList1.contains("数据库"));
//		// 函数参数为Collection类型
//		System.out.println("courseList1中是否有courseList2中的所有课程：" + courseList1.containsAll(courseList2));
//		System.out.println("移除courseList1中的数据库课程：" + courseList1.remove("数据库"));
//		System.out.println("移除courseList1中的网络安全课程：" + courseList1.remove("网络安全"));
//		// 求两个集合的交集，函数参数为Collection类型
//		System.out.println("保留courseList1中courseList2和courseList1都有的课程：" + courseList1.retainAll(courseList2));
		
		// 返回值类型是Object[]
		Object[] cl1 = courseList1.toArray();
		System.out.println("打印输出courseList1转换成数组后的数组中的所有课程：");
		for (int i = 0; i < cl1.length; i++) {
			System.out.println(cl1[i].toString());
		}
		
		System.out.println("打印输出courseList1中所有课程：" + courseList1);
		// 函数参数为Collection类型
		System.out.println("将courList2中的课程加入到courList1中:" + courseList1.addAll(courseList2));
		System.out.println("打印输出courseList1中所有课程：" + courseList1);
		// 返回值为void
		courseList2.clear();
		System.out.println("打印输出courseList2中所有课程：" + courseList2);
		

	}

}
