package chapter8collectionanditerator;

import java.util.Comparator;

public class CourseComparatorImp implements Comparator {
	@Override
    public int compare(Object arg0, Object arg1) {
          if(((String)arg0).length()<=((String)arg1).length()) 
      { 
          return -1;// 返回-1表示不改变顺序
      }
      else{
          return 1;// 返回1表示改变顺序
      }
    }
}
