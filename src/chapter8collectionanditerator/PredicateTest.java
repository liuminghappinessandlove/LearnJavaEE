package chapter8collectionanditerator;

import java.util.ArrayList;
import java.util.Collection;
import java.util.function.Predicate;

public class PredicateTest {

	public static void main(String[] args) {
		Collection courseList1 = new ArrayList();		
		courseList1.add("数据库");
		courseList1.add("JavaEE");
		courseList1.add("JavaSE");
		courseList1.add("高等数学");
		courseList1.add("大学英语");
		courseList1.add("线性代数");
		
		//批量删除符合条件的项，使用Lambda表达式，目标类型是Predicate
		courseList1.removeIf(course -> ((String)course).length()<=3);
		System.out.println(courseList1);
		
		//统计含有Java子串的课程个数
		System.out.println(calAll(courseList1, c -> ((String)c).contains("Java")));
		//统计长度大于5的课程个数
		System.out.println(calAll(courseList1, c -> ((String)c).length()>5));
	}

	public static int calAll(Collection courses, Predicate p){
		int total = 0;
		for (Object c : courses){
			if(p.test(c))
				total ++;
		}
		return total;
	}
}
