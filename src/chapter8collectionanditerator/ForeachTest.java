package chapter8collectionanditerator;

import java.util.Collection;
import java.util.HashSet;

public class ForeachTest {

	public static void main(String[] args) {
		Collection courseList1 = new HashSet();
		courseList1.add("数据库");
		courseList1.add("JavaEE");
		courseList1.add("高等数学");
		courseList1.add("大学英语");
		courseList1.add("线性代数");
		
		for (Object course : courseList1){
			String o = (String)course;
			if (o.equals("线性代数")) {
				// 这里同样不能使用remove函数，会引发ConcurrentModificationException异常
				courseList1.remove(course);
			}
		}
		
		System.out.println(courseList1);

	}

}
