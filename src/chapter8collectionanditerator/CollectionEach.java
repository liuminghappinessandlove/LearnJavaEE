package chapter8collectionanditerator;

import java.util.Collection;
import java.util.HashSet;

public class CollectionEach {

	public static void main(String[] args) {
		Collection courseList1 = new HashSet();
		courseList1.add("数据库");
		courseList1.add("JavaEE");
		courseList1.add("高等数学");
		courseList1.add("大学英语");
		courseList1.add("线性代数");
		
		// Lambda表达式的目标类型是Comsumer类型
		// 多个语句可写成 courseList1.forEach((course) -> {System.out.println(course);System.out.println(course);});
		 courseList1.forEach(course -> System.out.println(course));

	}

}
